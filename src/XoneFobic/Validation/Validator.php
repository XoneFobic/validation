<?php namespace XoneFobic\Validation;

use Illuminate\Validation\Factory;
use XoneFobic\Validation\Exceptions\ValidationException;

/**
 * Class Validator
 *
 * @package XoneFobic\Validation
 */
abstract class Validator {

    /**
     * @var \Illuminate\Validation\Factory
     */
    protected $factory;

    /**
     * @var
     */
    protected $errors;

    /**
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param      $attributes
     * @param null $identify
     *
     * @return bool
     * @throws Exceptions\ValidationException
     */
    public function validate($attributes, $identify = null)
    {
        $messages = isset(static::$messages) ? static::$messages : [];

        $validator = $this->factory->make($attributes, $this->prepareRules($identify), $messages);

        if ($validator->fails())
        {
            $this->errors = $validator->messages();

            throw new ValidationException($this->getErrors());

            return false;
        }

        return true;
    }

    /**
     * @param $identify
     *
     * @return array
     */
    private function prepareRules($identify)
    {
        $rules = [];

        foreach ( static::$rules as $key => $rule )
        {
            $rules[$key] = str_replace('{id}', $identify, $rule);
        }

        return $rules;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

} 
